# Tearduino

Tearout of the Arduino programming environment.

## Purpose

This is a *"Tearout"* of the Arduino programming environment directed towards
**eliminating** the *arduino builder*, to reintroduce standard programming habits,
such as using make and its conditional recompilation of object files.

The disadvantage of the *arduino builder* is that it puts object files in a
temporary directory that are deleted when exiting the Arduino IDE. Another
lesser known disadvantage is that tabs (counterparts .h-file management in pure
C) are processed in sorting order, making a systematic modularization of Arduino
code very awkward, and not really viable when the number of modules grow. 

Tearduino aims at making the transition from Arduino to pure C (or C++) easier.