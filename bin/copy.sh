#!/bin/bash
DEST=$PWD
if [ "$#" = "0" ]; then
	echo "Call $0 with the Arduino path as argument"
fi
if [ ! -f "$1/arduino" ]; then
	echo "Not an arduino path"
fi
######## include ########
#### Arduino.h ####
cd $1/hardware/arduino
tar cfz $DEST/include/dump01.tgz ./avr/cores/arduino/*.h
cd $DEST/include
tar xfz dump01.tgz
rm dump01.tgz
#### avr/ ####
cd $1/hardware/tools/avr/avr/include/
tar cfz $DEST/include/dump02.tgz ./avr/*.h
cd $DEST/include
tar xfz dump02.tgz
rm dump02.tgz
#### avr/variants/ ####
cd $1/hardware/arduino
tar cfz $DEST/include/dump03.tgz avr/variants
cd $DEST/include
tar xfz dump03.tgz
rm dump03.tgz
######## src ########
#### Arduino.c ####
cd $1/hardware/arduino
tar cfz $DEST/src/dump11.tgz ./avr/cores/arduino/*.c ./avr/cores/arduino/*.cpp ./avr/cores/arduino/*.S
cd $DEST/src
tar xfz dump11.tgz
#ls rm dump11.tgz
cp ../bin/copy/src-avr-cores-arduino-Makefile avr/cores/arduino/Makefile
